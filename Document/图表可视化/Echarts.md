# Echarts 官网

> 1. 官网: https://echarts.apache.org
> 2. 社区: https://www.makeapie.cn/echarts

# 安装命令

```sh
$ npm install echarts --save
```

# Echarts -- React

## 主页面

```js
// 主页面
import React, { useEffect, useState } from 'react';
// 柱形图
import { Pillar } from '../Components/Chart/Pillar';
import './index.scss';
export default function Chart() {
  // 传递过去的数据
  let [data] = useState([120, 200, 150, 80, 70, 110, 130]);
  useEffect(() => {
    Pillar('main', data); // 调用
  }, [data]);
  return (
    <div>
      <h1>Chart</h1>
      <div
        id='main'
        className='main'></div>
    </div>
  );
}
```

## 模块页面

```js
// 引入的柱形图数据
import * as echarts from 'echarts';
export const Pillar = (str, data) => {
  console.log(str, data);
  var myChart = echarts.init(document.getElementById(str));
  var option;
  option = {
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    },
    yAxis: {
      type: 'value',
    },
    series: [
      {
        data: data,
        type: 'bar',
      },
    ],
  };
  option && myChart.setOption(option);
};
```

> 代码

![image-20240324140705616](/public\images\Echarts\image-20240324140705616.png)

> 效果

![image-20240324141102739](/public\images\Echarts\image-20240324141102739.png)
