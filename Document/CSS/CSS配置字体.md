# CSS 配置字体

```html
<style>
    @font-face {
        /* 定义名称 */
        font-family: 'DouyinSansBold';
        /* 引入字体文件 */
        src: url(/DouyinSansBold.otf) format('opentype');
    }
    html {
        /* 应用字体 */
        font-family: 'DouyinSansBold';
    }
</style>

<body>
    <p>我是明楼</p>
    <h3>我喜欢学习</h3>
</body>
```

> 效果

![image-20240301152031138](/public/images/CSS/image-20240301152031138.png)
