# 媒体查询@Media

> 注意 and 等符号之间的间隔空格,没空格 会导致不生效

## 参数分析

```css
/* 
我的一点基础理解
第一个: 可以填 not only
第二个: 设备类型：screen 是手机 平板 电脑
第三个: 后面可以通过and 进行条件连接
*/
@media only screen and (max-width: 1300px) and (min-width: 500px) {
  p {
    font-size: 20px;
    color: orange;
  }
}
```

## 应用方式

### style+media-第一种

> style+media

```html
<style media="screen and (max-width:1100px) ">
  span {
    font-size: 40px;
    color: #cccccc;
  }
</style>
```

### link+media-第二种

> link+media

```css
<link media="screen and (max-width:1000px)" rel="stylesheet" href="./index.css">
```

### @media-第三种

> @media

```html
<style>
  @media screen and (max-width: 1300px) and (min-width: 500px) {
    p {
      font-size: 20px;
      color: orange;
    }
  }
</style>
```

### @import-第四种

> Css 中 @import 引入

```css
@import url(./pc.css) only screen and (orientation: landscape);
```

## 媒体设备

> 主要常用的是 screen

|   类型 | 说明                         |
| -----: | :--------------------------- |
|    all | 所有的媒体设备               |
| screen | 用于电脑端屏幕，平板，手机等 |
|  print | 打印设备                     |
| speech | 应用于屏幕阅读器等发声设备   |

> 有其他设备类型但均已废弃

## 逻辑判断

### 设备方向(横屏竖屏的判断)

```html
<!-- 竖屏生效 -->
<style media="screen and (orientation: portrait)">
  h1 {
    color: red;
  }
</style>
<!-- 横屏生效 -->
<style media="screen and (orientation: landscape)">
  h1 {
    color: blue;
  }
</style>
```

### AND 关键字(且)

> 添加条件 and 的意思 （且）

```html
<style>
  /* 最小宽度768px，最大宽度1000px  生效*/
  @media screen and (min-width: 768px) and (max-width: 1000px) {
    div {
      color: red;
    }
  }

  /* 最大宽度768px */
  @media screen and (max-width: 768px) {
    div {
      color: yellow;
      font-size: 12px;
    }
  }

  /* 大于1000px 最小像素为1000px */
  @media screen and (min-width: 1000px) {
    div {
      color: blue;
      font-size: 16px;
    }
  }
</style>
<div>我是钟明楼</div>
```

### NOT 关键字(非)

> 取反

```html
<style>
  /* 取反的意思 */
  /* 本意是生效范围最大是1000px */
  /* NOT后反过来，变为了 最小1000px */
  @media not screen and (max-width: 1000px) {
    div {
      font-size: 50px;
      color: red;
    }
  }
</style>
```

### Only 关键

> 支持媒体查询的浏览器生效, 不支持媒体查询的浏览器不生效

```html
<style>
  /* 支持媒体查询的浏览器生效, 不支持媒体查询的浏览器不生效 */
  @media only screen and (min-width: 1000px) {
    div {
      font-size: 30px;
      color: red;
    }
  }
</style>
```

### 媒体查询常用条件

|                            类型 | 说明                          |
| ------------------------------: | ----------------------------- |
| orientation:landscape\|portrait | landscape 横屏，portrait 竖屏 |
|                           width | 设备宽度                      |
|                          height | 设备高度                      |
|                       min-width | 最小宽度                      |
|                       max-width | 最大宽度                      |
|                      min-height | 最小高度                      |
|                      max-height | 最大高度                      |

## 实际应用-文件结构

> 样式表一

```css
/* pc.css */
div {
  font-size: 20px;
}
```

> 样式表二

```css
/* phone.css */
div {
  font-size: 30px;
}
```

> 配置的文件

```css
/* PC端 跟判断条件*/
@import url(./pc.css) only screen and (orientation: landscape);
/* 移动端 */
@import url(./phone.css) only screen and (orientation: portrait);
```

> 引入配置文件

```css
<link rel="stylesheet" href="./media.css">
/* import '/media.css' */
```

## 效果

![alt text](/public/images/CSS/媒体查询.png)

![alt text](/public/images/CSS/媒体查询_02.png)

![alt text](/public/images/CSS/媒体查询_01.jpg)
