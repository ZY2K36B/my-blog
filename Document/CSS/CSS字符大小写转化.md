# CSS 完成单词的大写转化

> text-transfrom

```html
<style>
  .one {
    /* 首字母大写 */
    text-transform: capitalize;
  }

  .two {
    /* 全单词大写 */
    text-transform: uppercase;
  }

  .three {
    /* 小型的大写 全单词大写 */
    font-variant: small-caps;
  }
</style>

<body>
  <h2 class="one">zhong ming lou</h2>
  <h2 class="two">zhongminglou</h2>
  <h2 class="three">zhongminglou</h2>
</body>
```

> 效果

![image-20240301143427619](/public/images/CSS/image-20240301143427619.png)
