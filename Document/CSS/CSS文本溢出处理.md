---
可以进行text-overflow:ellipsis文本溢出处理，显示省略号，或者 直接overflow:hidden 隐藏
---

# 文本溢出处理

```html
<style>
    /* 第一种方式 */
    div {
        width: 200px;
        border: 1px solid #b0b0b0;
        overflow: hidden;
        /* 禁止换行 */
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    /* 第二种方式 */
    p {
        width: 200px;
        border: 1px solid #b0b0b0;
        white-space: nowrap;
        overflow: hidden;
    }
</style>
</head>

<body>
    <div>
        文本溢出处理文本溢出处理文本溢出处理文本溢出处理
    </div>
    <p>
        文本溢出处理文本溢出处理文本溢出处理文本溢出处理
    </p>
</body>
```

> 效果

![文本溢出处理](/public/images/CSS/文本溢出处理.png)
