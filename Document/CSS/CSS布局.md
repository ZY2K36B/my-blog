> 圣杯布局 -- 双飞翼布局 -- 三栏布局

# BFC

> 产生 BFC 的元素

```sh
1. 浮动元素  	float不为 none
2. 定位元素  	position
3. overflow	   overflow为auto || hidden || scroll。 也就是不为visible
4. display
5. flex布局
5. grid布局
```

# 高度塌陷

> 解决方案

```
1. overflow：hidden 或 overflow:auto,让塌陷元素也参与进BFC
2. 伪元素clearfix类
3. 直接给父容器设置固定高度
4. flexbox布局
5. Grid布局
6. 终极方案：不使用浮动，改用其他定位方式 position 大法好

注意：
	考虑到兼容性 overflow: hidden
	现代web开发 推荐使用 clearfix类 || flex || grid
```

> 伪元素 clearfix 类

```css
.parent::after {
    content: '';
    display: block;
    clear: both;
}
```

# 圣杯布局

```sh
1. 通过float浮动+positons定位实现
2. 大致就是 通过父标签 设置左右两侧 padding 预留两侧面标签位置
3. 中间左中右三栏通过float:left进行浮动，
4. left设置 margin-left:-100% (百分比是相对于父元素的，所以会直接与center重叠)
	通过定位 left:-自身宽度，向左移动自身宽度的身位，占据之前padding预留的位置
5. right设置 margin-left：-自身宽度，BFC的margin会相互重叠会与center左侧重叠
	通过定位 right:-自身宽度,想右移动自身宽度的身位，占据之前padding预留的位置
6. center设置为100%直接占据全部内容即可
```

> 效果

![image-20240409141608446](/public/images/CSS布局\image-20240409141608446.png)

```html
<!DOCTYPE html>
<head>
    <title>标题</title>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        .header,
        .bottom {
            width: 100%;
            height: 50px;
            text-align: center;
        }

        .header {
            background-color: rgb(255, 146, 146);
        }

        .bottom {
            background-color: rgb(127, 127, 237);
        }

        .box {
            height: 500px;
            padding: 0px 200px;
        }

        .column {
            height: 500px;
        }

        .center {
            float: left;
            width: 100%;
            background-color: #b2b2f2;
        }



        .left {
            float: left;
            width: 200px;
            /* -100% 贴到左侧边框位置，再定位减去自身的宽度*/
            margin-left: -100%;
            position: relative;
            left: -200px;
            background-color: #b2f2f2;
        }

        .right {
            float: left;
            width: 200px;
            background-color: #f2b2f2;
            /* BFC 块级格式化上下文
                - 两个BFC的margin会重叠，一大一小，取大，两个margin为负，取绝对值大的，一正一负，进行相加(避免自己犯蠢， 2 + -1 = 1 实质是相减)
            */

            margin-left: -200px;
            position: relative;
            /*通过定位 right:-自身宽度,想右移动自身宽度的身位，占据之前padding预留的位置*/
            right: -200px;
        }
    </style>
</head>

<body>
    <div class="header">头部</div>
    <div class="box">
        <div class="column center">center</div>
        <div class="column left">left</div>
        <div class="column right">right</div>
    </div>
    <div class="bottom">底部</div>
</body>
</html>
```

# 双飞翼布局

```sh
1. 与圣杯布局类似, 但是不再使用postion 而是内容区域多了一层div,对内容区域设置左右padding,
	避免被遮挡内容
2. 使用float + 负margin的形式实现
```

> 效果图  
> 肯定会有困惑 感觉和圣杯布局一样 不同的地方在于 中间部分 双飞翼是四个 div 圣杯布局是三个 div
> 双飞翼优势在于不要使用定位了，使用 float + 负 margin 的形式实现

![image-20240409155104358](/public/images/\CSS布局\image-20240409155104358.png)

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0" />
        <title>双飞翼布局</title>
        <style>
            * {
                padding: 0;
                margin: 0;
                text-align: center;
            }

            .header,
            .bottom {
                width: 100%;
                height: 50px;
                text-align: center;
            }

            .header {
                background-color: rgb(255, 146, 146);
            }

            .bottom {
                background-color: rgb(127, 127, 237);
                border: 1px solid #000;
                overflow: hidden;
            }

            .bottom:after {
                content: '';
                display: block;
                clear: both;
            }

            .column {
                height: 500px;
            }

            .center {
                float: left;
                background-color: #b2b2b2;
                width: 100%;
            }

            .inside {
                background-color: #b2b2f2;
                width: 100%;
            }

            .left {
                float: left;
                background-color: #b2f2f2;
                width: 200px;
                margin-left: -100%;
            }

            .right {
                float: left;
                background-color: #f2b2f2;
                width: 200px;
                margin-left: -200px;
            }
        </style>
    </head>

    <body>
        <div class="header">头部</div>
        <div class="box">
            <div class="column center">
                <div class="column inside">center</div>
            </div>
            <div class="column left">left</div>
            <div class="column right">right</div>
        </div>
        <div class="bottom">底部222313</div>
    </body>
</html>
```

# 普通三栏布局

> 实现方式

```sh
1. flex布局
2. grid布局
3. float
4. 定位postion
5. table布局
```

# 末端
