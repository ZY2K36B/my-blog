# Ajax技术

> Ajax（Asynchronous JavaScript and XML）是一种用于创建动态、交互式网页应用的开发技术
>
> 核心在于利用浏览器内置的 XMLHttpRequest 对象或者后来新增的 Fetch API，实现客户端与服务器之间异步的数据通信。



# 解决方案

# 原生Ajax

> 核心在于浏览器内置的 XMLHttpRequest对象

> 创建XMLHttpRequest对象 ---  open --- send ---  onreadystatechange

```js
function http(method,url,success,fail){
    // 实例化
    let xhr = new XMLHttpRequest()
    
    // 初始化一个新的 HTTP 请求
    //参数：请求方式  请求地址  是否异步
    xhr.open('get','http://127.0.0.1:3000/',true) 
    if(method === 'GET'){
   		 xhr.send() // 将请求发送到服务器
    }
    
    if(method === 'POST'){
        //post 请求需要配置 setRequestHeader
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    	// 将请求发送到服务器 可以接收参数
        xhr.send("name='明楼'&age=26") //接收表单和字符串
    }

    // 事件处理  在XMLHttpRequest对象中的 readyState属性变化时会触发onreadystatechange事件
    xhr.onreadystatechange = () =>{
        // 判断readyState阶段===4 && 状态为200
        if(xhr.readyState === 4 && xhr.status === 200){
            // 成功回调
            return success && success(JSON.parse(xhr.responseText))
        }else{
            // 失败的回调
            return fail && fail(JSON.parse(xhr.status))
		}
    }
}

```



# Axios

## 基础使用

### .then

```js
function getRequest() {
  axios.get('http://localhost:3000/',{
      params:{
          name:'明楼'
          age:26
      }
  }).then((response) => {
    console.log(response.data);
  });
}

function postRequest() {
  axios.post('http://localhost:3000/',{
      name:'明楼'
      age:26
  }).then((response) => {
    return console.log(response.data);
  });
}
```



### async/await

```js

async function getRequest() {
  let { data } = await axios.get('http://localhost:3000/',{
      params:{
          name:'明楼'
          age:26
      }
  });
  return console.log(data);
}

async function postRequest() {
  let { data } = await axios.post('http://localhost:3000/', {
    name: '明楼',
    age: 26,
  });
  return console.log(data);
}

```



## aixos的二次封装

### 基地址

> 可以通过axios.create({}) 创建一个新的实例  
>
> 参数：   baseURL(基地址)

```js
let http = axios.create({
    baseURL:"http://127.0.0.1:3000"
})
```



### 拦截器

> 通过属性  interceptors【因ter 塞普ter 斯】

```js
// 请求拦截器
http.interceprot.request.use((config)=>{
    console.log('拦截了请求')
	return config
})

// 响应拦截器
http.interceprot.response.use((config)=>{
    console.log('拦截了请求')
	return config
})
```





# Fetch API

> 使用  fetch() 进行

> fetch(‘url请求的地址’).then(res=>返回的类型).then(res=>{真正的值})
>
> fetc(‘url’).then(res=>res.json).then(res=>{console.log(res)})

```js
// 默认为 GET请求
fetch('http://127.0.0.1:3000/?name="明楼"')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
    })
    .then((res) => {
      console.log(res.data);
    });
}

// POSt 请求
fetch('http://127.0.0.1:3000', {
    method: 'POST', // 请求类型
    headers: {
      'Content-Type': 'application/json', // 请求头
    },
    body: JSON.stringify({
      name: '明楼',
      age: 26,
    }),
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
    })
    .then((res) => {
      console.log(res.data);
    });
```



