# Promise

## 基本流程

```sh
							--- 成功(fulfilled)---执行回调
new Promise() -- 执行异步（两种结果）					---- 返回新的Promise
							--- 失败(rejected) --- 执行回调
```



## 属性和方法

|       命令 | 说明                                                         |
| ---------: | ------------------------------------------------------------ |
|      .then | 链式调用，接收俩个回调函数（resolve=>{}，reject=>{}）成功和失败的回调 |
|     .catch | 执行失败的回调，如果then处理的失败 .catch就不会执行了        |
|   .finally | 结束后执行的回调，可以用来执行清除操作                       |
|        all | 接收一个数组成功返回成功数组，失败执行失败回调               |
| allSettled | 接收一个数组，返回数组[{status:”fufilled”,value:”1”},{status:”rejected”,reason:”2”},{status:”fufilled”,value:”3”}] |
|        any | 接收一个数组，返回第一个成功的回调，<br />全失败rejected返回：All promises were rejected |
|       race | 接受一个数组，赛跑，谁先成功返回谁，无论成功失败             |



## 基础使用

```js
// Promise
    // Promise() then() catch() finally(执行完毕)
    let a = new Promise(function (resolve, reject) {
        setTimeout(() => {
            console.log('第一阶段完成');
            reject('执行成功')
        }, 1000);
    }).then((resolve) => {
        // 成功的
        console.log('resolve', resolve);
    }, (reject) => {
        // 失败的   只会执行一次失败  执行了这个就不会执行catch
        console.log('reject', reject);
        // 执行失败的回调
    }).catch((err) => {
        console.log("catch", err);
       //  结束前执行
    }).finally((info) => {
        console.log("finally", info);
    })
```





## Promise.all()

> Promise.all    一个为rejected 返回为rejected

```js
let p1 = new Parmise((resolve,reject)=>{
    resolve(1)
})

let p2 = Parmise.resolve(2)  // 一个成功的parmise

let p3 = 3  // 会被当做 resolve(3)

Parmise.all([p1,p2,p3]).then((resolve)=>{
    console.log(resplve) // [1,2,3]
},(reject)=>{
    console.log(reject) // 哪个失败返回哪个 比如p2 失败了 返回 2
})
```



## Promise.allSettled()

> 只有成功回调，接受一个数组，返回一个数组包对象的格式，有着成功或失败的结果
>
> [{status:"fulfilled":value:”1”},{status:”rejected”,reason:”2”},{status:”rejected”,reason:”3”}]

```js
let p1 = new Promise((resolve,reject)=>{
    resolve(1)
})

let p2 = Promise.reject(2)

let p3 = 3

Promise.allSettled([p1,p2,p3]).then(
    resolve=>{
    	console.log(resolve)
	},
    reject=>{
    	console.log(reject)
	}
)
```





## Promise.any()

> 接收一个数组 ，有一个成功就执行成功回调，不再向下执行，返回成功的回调
>
> 如果全部失败会提示 All promises were rejected

```js
/ Promise race  谁第一个返回 就用谁  无论成功还是失败
let p1 = new Promise((res, rej) => {
    rej('我是p1')
})

let p2 = Promise.resolve('我是p2') // 返回 p2

let p3 = Promise.reject('我是p3')

Promise.any([p1, p2, p3]).then(resolve => {
    console.log(resolve);
}).catch(err => {
    console.log(err);
})
```





## Promise.race

> 接收一个数组，谁先完成调用谁，无论成功失败

```js
/ Promise race  谁第一个返回 就用谁  无论成功还是失败
let p1 = new Promise((res, rej) => {
    setTimeout(() => {
        rej('我是p1')
    }, 0)
})

let p2 = Promise.resolve('我是p2') // 返回 p2

let p3 = Promise.reject('我是p3')

Promise.race([p1, p2, p3]).then(resolve => {
    console.log(resolve);
}).catch(err => {
    console.log(err);
})
```

