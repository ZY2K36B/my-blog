# 什么是 Mock.js

> Mock.js 是模拟数据的第三方库，用于项目中可以拦截请求，构造假数据
>
> 官方文档：http://mockjs.com/

# Mock 基础使用

## 安装配置

> VSCode 打开空文件 ===> 安装命令 ===> 建 js 文件 ===> 代码 ===> 运行命令

```sh
yarn add mockjs
```

## 基础使用

```sh
const Mock = require('mockjs')
const Random = Mock.Random

function Fn() {
    const arr = []
    // 生成10条数据
    for (let i = 0; i < 10; i++) {
        arr.push({
            id: i + 1,                              // 生成id
            name: Random.cname(),                   // 生成随机姓名
            price: Random.integer(100, 1000),       // 生成随机价格
            addredss: Mock.mock('@county(true)'),   // 生成地址
        })
    }
    return {
        // 返回一个路由列表
        arr
    }
}

module.exports = Fn;
```

## 启动

> json-server 启动命令 --port 端口 -- host 主机 环回地址 文件名

```sh
json-server --watch --port 6969 --host 127.0.0.1 db.js
```

# 常用类型

```js
const Mock = require('mockjs');
let data = Mock.mock({
    'list|3': [
        {
            id: '@id',
            姓名: '@cname',
            地址: '@county(true)',
            时间: '@datetime',
            年龄: '@integer(18, 60)',
            性别: '@boolean',
            头像: '@image(100x100, @color, @cname)',
            电话: '@integer(13000000000, 19999999999)',
            邮箱: '@email',
            账号: '@word(6, 12)',
            密码: '@word(6, 12)',
            '爱好|1': ['吃饭', '睡觉', '打豆豆'],
            '状态|1': ['正常', '禁用'],
            '角色|1': ['超级管理员', '管理员', '普通用户'],
            备注: '@cparagraph(1, 2)',
        },
    ],
});
```

# > 实用部分

> mockjs 可以把项目中的 `指定请求` 给 `拦截`

> 在项目中安装 mockjs

```sh
npm i mockjs @types/mockjs -D
```

> src/mock/index.js 文件编写

```js
import { mock } from 'mockjs';
let { data } = mock({
    'data|1000': [
        {
            'id|+1': 1,
            name: '@cname',
            'age|18-30': 1,
            address: '@county(true)',
            email: '@email',
            phone: '@integer(13000000000, 18999999999)',
            date: "@datetime('yyyy-MM-dd')",
            time: "@time('HH:mm:ss')",
        },
    ],
});

//
mock("'http://localhost:3000/vue/list'", 'get', function (request) {
    console.log(request.body);
    return {
        code: 200,
        data: data,
        msg: '请求成功',
    };
});
mock(/.*?\/vue\/list/, 'post', function (request) {
    let { name, age } = JSON.parse(request.body);
    console.log(name, age);
    return {
        code: 200,
        data: data,
        msg: '请求成功',
    };
});
mock(/.*?\/vue\/list/, 'put', function (request) {
    console.log(request.body);
    return {
        code: 200,
        data: data,
        msg: '请求成功',
    };
});
mock(/.*?\/vue\/list/, 'delete', function (request) {
    console.log(request.body);
    return {
        code: 200,
        data: data,
        msg: '请求成功',
    };
});
```

![image-20240410220254854](/public/images/MockJS/image-20240410220254854.png)

> main.js || main.ts

```js
// 引入mock  启用假数据   引入就会被编译生效
import '@/utils/mock/mock.js';
```

![image-20240410220020929](/public/images/MockJS/image-20240410220020929.png)

# 项目中使用

> 拦截 axios 请求 不让它发出去了

![image-20240410115340994](/public/images/MockJS/image-20240410115340994.png)

## 基础写法

> 默认是 get 请求
>
> 问题：每次请求都是不同的新建数据 且 GET POST 等请求都可以访问

```js
mock('url', '请求类型',{'返回的响应'})
```

> 正则 url 模糊匹配 请求 /vue/list

```js
mock(/.*?\/vue\/list/, '请求类型',{'返回的响应'})
```

> 函数写法 可以拿到 body type url 属性

```js
mock(/.*?\/vue\/list/, '请求类型',function(req){ return {'返回的响应'}})
```

## 参考代码

```js
import axios from 'axios'; // 导入的axios
import { mock } from 'mockjs'; //引入mock

const server = axios.create({
    baseURL: 'http://localhost:3000/',
    timeout: 5000 * 10,
    headers: { 'Content-Type': 'application/json' },
});

// 个人觉得放在这里更有对比   同样的请求  会被 mock给拦截，不发出去而是走假数据
mock('http://localhost:3000/vue/list', {
    code: 200,
    'data|10': [
        {
            'id|+1': 1,
            name: '@cname',
            'age|18-30': 1,
            address: '@county(true)',
            email: '@email',
            phone: '@integer(13000000000, 18999999999)',
            date: "@datetime('yyyy-MM-dd')",
            time: "@time('HH:mm:ss')",
        },
    ],
    msg: '响应成功',
});

// 无关紧要的拦截请求
server.interceptors.request.use(
    (config) => {
        return config;
    },
    (err) => {
        return Promise.reject(error);
    },
);

// 无关紧要的拦截响应
server.interceptors.respens.use(
    (response) => {
        return response;
    },
    (err) => {
        return Promise.reject(error);
    },
);
```

## 完整写法

```js
let { data } = mock({
    'data|10': [
        {
            'id|+1': 1,
            name: '@cname',
            'age|18-30': 1,
            address: '@county(true)',
            email: '@email',
            phone: '@integer(13000000000, 18999999999)',
            date: "@datetime('yyyy-MM-dd')",
            time: "@time('HH:mm:ss')",
        },
    ],
});

// MockJs 假数据
mock(/.*?\/vue\/list/, 'post', function (request) {
    let { name, age } = JSON.parse(request.body);
    let data = data;
    return {
        code: 200,
        data: data,
        msg: '请求成功',
    };
});
```

## 增删改查

```sh
import { mock } from "mockjs";
let { data } = mock({
    "data|10": [{
        "id|+1": 1,
        "name": "@cname",
        "age|18-30": 1,
        "address": "@county(true)",
        "email": "@email",
        "phone": "@integer(13000000000, 18999999999)",
        "date": "@datetime('yyyy-MM-dd')",
        "time": "@time('HH:mm:ss')",
    }],
})

// get
mock(/.*?\/vue\/list/, "get", function (request) {
    console.log(request.body);
    return {
        "code": 200,
        data: data,
        "msg": "请求成功",
    }
})

// post
mock(/.*?\/vue\/list/, "post", function (request) {
    let { name, age } = JSON.parse(request.body)
    console.log(name, age);
    return {
        "code": 200,
        data: data,
        "msg": "请求成功",
    }
})

// put
mock(/.*?\/vue\/list/, "put", function (request){})

// delete
mock(/.*?\/vue\/list/, "delete", function (request) {})
```

# 数据定义规范

> 启动命令 node +文件名 在终端打印

```sh
node .\Server.js
```

> 非 json-server log 输出

```js
const Mock = require('mockjs');

let data = Mock.mock({
    'list|4': [
        {
            // 4个对象
            'id|+1': 1,
        },
    ],
});
// 在终端打印
console.log(data.list);
```

## 数字 || 字符串

```javascript
// 值是数字 || 字符串的情况
let data = Mock.mock({
    'list|2': [
        {
            '0|+1': 1, // 从1开始累加 每条数据+1
            '1|5': 1, // 五个1相加        === 5
            '2|5': '1', // 五个字符串1相加  ==='11111'
            '3|1-10': '1', // 生成随机数，然后再与1相加
            '4|1-100': 1, // 随机生成一个1-100之间的数字,然后与1相加得出随机数
            '5|1': ['1', '2'], // 随机取一个，从数组中 除了1其他数字会  如为2的话: [ '1', "2", '1', "2",]
            '6|1-10.3': 1, // 随机生成一个1-10之间的数字，保留三位小数
            '7|1-10.2-3': 1, // 随机生成一个1-10之间的数字，保留两到三位小数
        },
    ],
});
```

## 布尔

```js
// 值为布尔值
let data = Mock.mock({
    'list|4': [
        {
            'id|+1': 1,
            // 二分之一的几率是true ,数字多少不影响二分之一的几率是true
            '布尔|1': true,
            //两者作用一样
            '数组布尔|1': [true, false],
        },
    ],
});
```

## 对象

```js
// 值为对象
let data = Mock.mock({
    'list|4': [
        {
            // 4个对象
            'id|+1': 1,
            '对象|2-3': {
                // 取对象中2-3个键值对
                name: '@name', // 随机生成英文姓名  中文是cname
                age: 18, // 18
                'gender|1': ['男', '女'], // 随机取1个值
            },
        },
    ],
});
```

## 数组

> 值为 1 数组中随机取 1 个， 除 1 之外， 其他都是重复多少次

```js
// 值为数组
let data = Mock.mock({
    'list|4': [
        {
            // 4个对象
            'id|+1': 1,
            '数组1|1': ['a', 'b', 'c'], // 随机取1个值
            '数组2|2': ['明楼', 26], // 非1会 比如是2: ['明楼', 26,'明楼', 26]   其他都是重复多少次
        },
    ],
});
```

## 函数

> 函数 return 返回的是什么就是什么 不受 |xxx 的影响

```js
// 值为函数
let data = Mock.mock({
    'list|4': [
        {
            // 4个对象
            'id|+1': 1,
            // 函数不受 |xxx 影响  return是什么就是什么
            '函数|2': function () {
                return ['a', 'b'];
            }, // 结果为 ['a', 'b']
        },
    ],
});
```

## 正则表达式

> 值如果是正则表达式，返回的就是符合这个正则表达式的值

```js
// 值为正则表达式
let data = Mock.mock({
    'list|4': [
        {
            // 4个对象
            'id|+1': 1,
            身份证号: /^[1-9]\d{7}(?:0\d|10|11|12)(?:0[1-9]|[1-2][\d]|30|31)\d{3}$/,
            IP地址: /^((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])(?::(?:[0-9]|[1-9][0-9]{1,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]))?$/,
            电话号码: /^1[34578]\d{9}$/,
            邮箱: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        },
    ],
});
```

# 数据占位符

> 引用的是 Mock.Random 中的方法 ===> 数据占位符指的是 ‘@xxx’

```js
const Random = Mock.Random; // 进行引用
```

## 图片

> 使用的网站 https://dummyimage.com/

```js
let data = Mock.mock({
    'img|1': [
        {
            // 默认是随机生成
            img: '@image',
            // 两个是相同的
            RandomIng: Random.image(),
            // 大小100*100，随机背景颜色，随机字体颜色，随机中文人名
            图片: '@image(100x100, @color,@color,@cname)',
            图片1: Random.image('100x100', '@color', Random.color(), Random.cname()),
        },
    ],
});
```

## 日期

```js
// 日期
let data = Mock.mock({
    'date|1': [
        {
            日期: '@date',
            现在的日期: '@now',
            随机日期: '@datetime',
            // 自定义方式
            年月日: '@now(yyyy/MM/dd HH:mm:ss)',
        },
    ],
});
```

## 城市 县区 区号 邮编

```js
// 城市 县区 区号 邮编
let city = Mock.mock({
    'date|1': [
        {
            'id|+1': 1,
            市: '@city',
            省市: '@city(true)',
            省: '@province',
            省市县: '@county(true)',
            区县: '@county',
            区号: '@integer(10000, 99999)',
            邮编: '@integer(100000, 999999)',
        },
    ],
});
```

## 经纬度

```js
// 经纬度
let LaL = Mock.mock({
    'date|1': [
        {
            经度: '@float(-180, 180, 2, 2)',
            纬度: '@float(-90, 90, 2, 2)',
        },
    ],
});
```

## 邮箱

```js
// 邮箱
let Email = Mock.mock({
    'date|1': [
        {
            邮箱: '@Email',
            QQ邮箱: '@Email(qq.com)',
            网易邮箱: '@Email(163.com)',
        },
    ],
});
```
