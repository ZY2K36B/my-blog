# 安装 Express 框架

> 安装命令 Exprss 要有 node 环境
>
> exprss 名称

```sh
$ express newExprss
```

# 打开项目

![image-20240320151356589](/public/images/框架/Express/image-20240320151356589.png)

# 安装依赖

> 跨域 Cors

```sh
$ npm i cors
```

> MongoDB

```sh
$ npm i mongoose
```

> MySQL

```sh
$ npm install mysql2
```

# MongoDB 配置

## 创建数据库连接

> 连接数据库 model / db.js

```js
const mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/db0000');
const conn = mongoose.connection;
conn.on('open', () => {
  console.log('数据库连接成功....');
});
conn.on('err', (err) => {
  console.log('数据库连接失败....', err);
});

module.exports = mongoose;
```

## 配置字段

> 配置字段

```js
const mongoose = require('./db');
// 配置字段  不配置 上传不上去
const listSchema = new mongoose.Schema({
  name: String,
  age: Number,
  // 完整写法
  sex: {
    type: String,
    default: '男',
  },
});

const listModel = mongoose.model('list', listSchema, 'list');

module.exports = { listModel };
```

## 效果

![image-20240320151156657](/public/images/框架/Express/image-20240320151156657.png)

## 应用

> 使用 router / index.js

```js
var express = require('express');
// 引入
var { listModel } = require('../model/model');
var router = express.Router();
// 跨域cors
var cors = require('cors');
router.use(cors());
/* GET home page. */

// 应用
router.get('/', function (req, res, next) {
  res.send({
    code: 200,
    msg: '响应成功',
  });
});

module.exports = router;
```

## 请求方式

> 推荐使用 get post

### get

```js
router.get('/', function (req, res, next) {
  let data = req.query;
  res.send({
    code: 200,
    msg: '响应成功',
    data,
  });
});
```

### Post

```js
router.post('/list', async (req, res, next) => {
  let data = await listModel.find();
  res.send({
    code: 200,
    msg: '响应成功',
    data,
  });
});
```

### put

```js
router.put('/', async (req, res, next) {
  	let {name,age} = req.body
  	res.send({
   		code: 200,
    	msg: '响应成功',
    	name,
        age,
  })
})
```

### delete

```js
router.delete('/', function (req, res, next) {
  res.send({
    code: 200,
    msg: '响应成功',
  });
});
```

# MySQL 配置

> 连接数据库

```js
// db.js
const mysql = require('mysql2');

// 配置数据库连接信息
const pool = mysql.createPool({
  host: 'localhost', // 数据库服务器地址
  user: 'your_username', // 数据库用户名
  password: 'your_password', // 数据库密码
  database: 'your_database_name', // 数据库名
  waitForConnections: true, // 是否等待可用连接
  connectionLimit: 10, // 连接池大小
  queueLimit: 0, // 当连接池满时允许排队的连接数，默认为0表示无限制
});

module.exports = pool.promise(); // 使用Promise版本的连接池以简化异步操作
```

> 应用

```js
// app.js
const express = require('express');
const app = express();
const db = require('./db'); // 引入数据库连接

// 示例：获取数据
app.get('/users', async (req, res) => {
  try {
    const [rows, fields] = await db.query('SELECT * FROM users');
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send('Error fetching data from the database.');
  }
});

// 启动服务器
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
```

# 启动框架

> 可以使用 命令启动 如 nodemon
