---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: '2K36B的文档库'
  text: '备忘录'
  tagline: '我喜欢学习，也喜欢分享'
  image:
    src: '/休息时间.png'
    alt: '背景图'
  actions:
    - theme: brand
      text: 出发
      link: /前言
    - theme: alt
      text: 开始
      link: /前言

features:
  - title: 🤔 目的
    details: 想要什么
  - title: 🎯 目标
    details: 为了什么
  - title: 🚀 方向
    details: 该做什么
---

<style>
:root {
  --vp-home-hero-name-color: transparent;
  --vp-home-hero-name-background: -webkit-linear-gradient(120deg, #bd34fe, #41d1ff);
}
</style>
