// let routerList = [
//   {text: '框架',Child:''},
// ]
export let router = [
  // 前言
  {
    text: '前言',
    items: [
      { text: '前言', link: '/前言.md' },
      { text: '我的技术栈', link: '/技术栈' },
    ],
  },
  // 框架
  {
    text: '框架',
    collapsed: false,
    collapsible: true,
    items: [
      { text: 'Express', link: '/Document/框架/Express.md' },
      { text: 'Vue2', link: '/Document/框架/Vue2.md' },
    ],
  },
  // JavaScript
  {
    text: 'JavaScript',
    collapsed: true,
    items: [
      { text: 'Ajax技术', link: '/Document/JavaScript/Ajax.md' },
      { text: 'Promise', link: '/Document/JavaScript/Promise.md' },
    ],
  },
  // CSS
  {
    text: 'CSS',
    collapsed: true,
    items: [
      { text: 'CSS布局', link: '/Document/CSS/CSS布局.md' },
      { text: 'CSS媒体查询', link: '/Document/CSS/CSS媒体查询.md' },
      { text: 'CSS配置字体', link: '/Document/CSS/CSS配置字体.md' },
      { text: 'CSS文本溢出', link: '/Document/CSS/CSS文本溢出处理.md' },
      { text: 'CSS字符大小写转化', link: '/Document/CSS/CSS字符大小写转化.md' },
    ],
  },
  // 图表可视化
  {
    text: '图表可视化',
    collapsed: true,
    items: [
      { text: 'Echarts', link: '/Document/图表可视化/Echarts.md' },
    ],
  },
  // 技术点
  {
    text: '技术点',
    collapsed: true,
    items: [
      { text: 'MockJS', link: '/Document/技术点/MockJS.md' },
      { text: '递归组件', link: '/Document/技术点/递归组件.md' },
      { text: '无感刷新', link: '/Document/技术点/无感刷新.md' },
      { text: '记录在线时间', link: '/Document/技术点/记录在线时间.md' },
      { text: '小功能', link: '/Document/技术点/小功能.md' },
      { text: '优化方式', link: '/Document/技术点/优化方式.md' },
    ],
  },
  // 知识点
  {
    text: '知识点',
    collapsed: true,
    items: [
      { text: 'Vue2', link: '/Document/知识点/Vue2-知识.md' },
      { text: 'JavaScript', link: '/Document/知识点/JavaScript.md' },
    ],
  },
  // 服务器
  {
    text: '服务器',
    collapsed: true,
    items: [
      { text: '阿里云+宝塔', link: '/Document/服务器/阿里云+宝塔.md' },
      { text: '线上项目的更新', link: '/Document/服务器/线上项目的更新.md' },
      { text: '上线后刷新404问题', link: '/Document/服务器/上线后刷新404问题.md' },
    ],
  },
  // 网络
  {
    text: '网络',
    collapsed: true,
    items: [{ text: '网络七层模型', link: '/Document/网络/网络七层模型.md' }],
  },
  // 架构模式
  {
    text: '架构模式',
    collapsed: true,
    items: [{ text: '微前端', link: '/Document/架构模式/微前端-无界.md' }],
  },
  // 工具
  {
    text: '工具',
    collapsed: true,
    items: [
      { text: 'MarkDown', link: '/Document/工具/MarkDown.md' },

    ],
  },
];
