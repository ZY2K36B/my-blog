import { defineConfig } from 'vitepress';
import { set_sidebar } from '../utils/auto-gen-sidebar.mjs'; //自动配置路由
import { router } from './router';
// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: '2K36B的文档网站',
  description: '2K36B的备忘录',
  lastUpdated: true,
  head: [['link', { rel: 'icon', href: '/休息时间.png' }]],
  themeConfig: {
    // 路由
    sidebar: router,

    logo: '/休息时间.png', // loge
    outlineTitle: '文章目录', // 左侧栏标题
    outline: [1, 4],
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      // 右上角路由
      { text: '主页', link: '/' },
      { text: '内容', link: '/前言.md' },
    ],

    returnToTopLabel: '返回顶部',
    sidebarMenuLabel: '文章目录',
    darkModeSwitchLabel: '切换显示模式',
    darkModeSwitchTitle: '切换到深色模式',
    lightModeSwitchTitle: '切换到浅色模式',

    lastUpdated: {
      text: '更新日期',
      formatOptions: {
        // dateStyle: 'full',
        // timeStyle: 'medium',
      },
    },
    // 上下页
    docFooter: {
      prev: '上一页',
      next: '下一页',
    },
    search: {
      provider: 'local',
      options: {
        translations: {
          button: {
            buttonText: '搜索文档',
            buttonAriaLabel: '搜索文档',
          },
          modal: {
            noResultsText: '无法找到相关结果',
            resetButtonTitle: '清除查询条件',
            footer: {
              selectText: '选择',
              navigateText: '切换',
            },
          },
        },
      },
    },
    footer: {
      // footer
      message: '感谢知识分享的每一位，向无私分享智慧的精神致谢，他们以知识之光照亮前行路，构筑人类进步阶梯',
      copyright:
        '<img style="width:12px; line-heigth:13px; display: inline; " src="/备案图标.png" alt="备案图标">  <a href="https://beian.mps.gov.cn/#/query/webSearch?code=13043102001019" rel="noreferrer" target="_blank" style="text-decoration: none">冀公网安备13043102001019</a>-<a href="https://beian.miit.gov.cn/" target="_blank" style="text-decoration: none">冀ICP备2024057486号</a> ',

      // https://vitepress.dev/reference/default-theme-config
    },

    socialLinks: [{ icon: 'github', link: 'https://github.com/vuejs/vitepress' }],
  },
});
